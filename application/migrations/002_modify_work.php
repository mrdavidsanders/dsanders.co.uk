<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Modify_work extends CI_Migration {

	public function up()
	{
		$_fields = Array
		(
		'uri' => array('type' => 'varchar',
		'constraint' => '255')
		);
				
		$this->dbforge->add_column('work', $_fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('work', 'uri');
	}
}