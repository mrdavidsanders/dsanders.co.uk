<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Modify_work_3 extends CI_Migration {

	public function up()
	{
		$_fields = Array
		(
		'parent_id' => array('type' => 'int',
		'constraint' => '11')
		);
		$this->dbforge->add_key('parent_id', TRUE);
		$this->dbforge->add_column('work', $_fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('work', 'parent_id');
	}
}