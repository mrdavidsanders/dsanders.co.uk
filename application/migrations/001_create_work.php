<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_work extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'title' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
			),
			'content' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('id', TRUE);		
		$this->dbforge->create_table('work');
	}

	public function down()
	{
		$this->dbforge->drop_table('work');
	}
}