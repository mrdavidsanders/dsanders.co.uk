<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Modify_work_4 extends CI_Migration {

	public function up()
	{
		$_fields = Array
		(
		'notes' => array('type' => 'text')
		);

		$this->dbforge->add_column('work', $_fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('work', 'notes');
	}
}