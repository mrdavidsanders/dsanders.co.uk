<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
		
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth/login');
		}
	}
	
	public function index() {
		$this->load->view('admin/header', Array("title" => "Work"));
		$this->load->view('admin/index');
		$this->load->view('admin/footer');
	}
	
	//Work Page
	public function getWorkItems() {
		$this->load->model('work_model');
		$data['work'] = $this->work_model->get_items();
		$this->load->view('admin/header', Array("title" => "Work"));
		$this->load->view('admin/getworkitems', $data);
		$this->load->view('admin/footer');
	}
	
	public function addWorkItem($data=NULL) {
		$this->load->model('work_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		
		//set validation fields
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('admin/header', Array("title" => "Work"));
			$this->load->view('admin/addworkitem');
			$this->load->view('admin/footer');
		}
		else {
			$this->work_model->title = $this->input->post('title');
			$this->work_model->content = $this->input->post('content');
			$this->work_model->addItem();
			redirect('admin/getWorkItems');
		}
	}

	public function editWorkItem($data=NULL) {
		$this->load->model('work_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	
		//set validation fields
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
	
		if ($this->form_validation->run() == FALSE) {
			$id = $this->uri->segment(3, NULL);
			$this->work_model->get_items($id);
			$data = get_object_vars($this->work_model);
			$this->load->view('admin/header', Array("title" => "Work"));
			$this->load->view('admin/editworkitem', $data);
			$this->load->view('admin/footer');
		}
		else {
			$this->work_model->id = $this->input->post('id');
			$this->work_model->title = $this->input->post('title');
			$this->work_model->content = $this->input->post('content');
			$this->work_model->updateItem();
			redirect('admin/getWorkItems');
		}
	}
	
	public function deleteWorkItem() {
		$id = $this->uri->segment(3, NULL);
		if (NULL === $id )
			throw new Exception("Bad Parameter");
		
		$this->load->model('work_model');
		$this->work_model->deleteItem($id);
		redirect('admin/getWorkItems');
	}
	
	//About page
	
}