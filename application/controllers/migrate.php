<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class migrate extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('migration');
		
		
		
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		
		if (!$this->ion_auth->is_admin()) {
			show_404();
		}
		
		
		 $this->migration->current();
		 die("Complete @ Version ".$this->migration->getCurrentVersion());
		
	}	
}	