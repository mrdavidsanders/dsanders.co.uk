<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class work extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('work_model');
	}
	
	public function index()
	{
		$data['work'] = $this->work_model->get_items();
		$this->load->view('header', Array("title" => "Work"));
		$this->load->view('work', $data);
		$this->load->view('footer');
	}
	
	public function view($id)
	{
		$data['work'] = $this->work_model->get_items($id);
	}
	
	public function other()
	{
		$this->load->view('header');
		$this->load->view('work');
		$this->load->view('footer');
		
	}
	
}

