<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<link rel="shortcut icon" href="http://dsanders.co.uk/favicon.ico" type="image/gif" />
<link rel="stylesheet" href="/assets/css/style.css" type="text/css" />
<title>David Sanders</title>
</head>
<body>
<div id="wrapper">
<div id="header">
<div id="lizziefpblock" style=" height: 100%; width: 100%; background-color: white; ">

<span style=" letter-spacing: -4px; margin-top: 30px; margin-left: 10px; margin-bottom: 0px; text-align: left; float: left; font-size: 55pt; color: grey;"><span style="color: black;">David</span> Sanders</span>
<div id="fpdiv"><span class="brownbox"><a target="_top" href="/about">About</a></span>&nbsp;&nbsp;<span class="boxlink"><a target="_top" href="/work">Work</a></span>&nbsp;&nbsp;<span class="boxlink"><a target="_top" href="/contact">Contact</a></span></div>
</div>


</div>

<div class="pagecontent">
<br />
<div style="color: black; opacity: 1.0; text-align: left;">
<h2>404!</h2>
Woah, that page doesn't exist!
<br /><br />
Try going back to the <a href="/">homepage</a>
<br /><br />	
</div>
</div>
<div id="footer">
&copy; David Sanders 2006 - 2014  

</div>
</div>
</body>
</html>
