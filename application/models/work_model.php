<?php

class Work_model extends CI_Model {

	public $id = NULL;
	public $title = NULL;
	public $content = NULL;
	
	public function __construct() {
		$this->load->database();		
	}
	
	public function get_items($id = FALSE) {
		if ($id === FALSE) {
			$query = $this->db->get('work');
			return $query->result_array();
		}
		$query = $this->db->get_where('work', array('id' => $id));
		$data = $query->row_array();
		$this->id = $data['id'];
		$this->title = $data['title'];
		$this->content = $data['content'];
		return $data;
	}
	
	public function addItem() {
		if (NULL === $this->title || NULL === $this->content)
			throw new Exception("Bad parameters");
		
		if ($this->db->query("INSERT INTO work (title, content) VALUES({$this->db->escape($this->title)}, {$this->db->escape($this->content)})")) {
		$this->id = $this->db->insert_id();
		return True;
		}
		
		return False;
	}
	
	public function updateItem() {
		if (NULL === $this->title || NULL === $this->content || NULL === $this->id)
			throw new Exception("Bad parameters");
	
		return $this->db->query("UPDATE work SET title = {$this->db->escape($this->title)}, content = {$this->db->escape($this->content)} WHERE id = {$this->id}");
	}
	
	public function deleteItem($id=NULL) {
		if (NULL === $id)
			throw new Exception("Bad Parameter");
		
		return $this->db->query("DELETE FROM work WHERE id = {$id}");
	}
	
}