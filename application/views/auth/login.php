<h1><?php echo lang('login_heading');?></h1><br />

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/login");?>
<table cellspacing=5>
  <tr><td>
    <?php echo lang('login_identity_label', 'identity');?></td>
    <td><?php echo form_input($identity);?></td>
  </tr>

  <tr>
    <td><?php echo lang('login_password_label', 'password');?></td>
    <td><?php echo form_input($password);?></td>
  </tr>

  <tr>
    <td colspan=2><?php echo lang('login_remember_label', 'remember');?>
    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?></td>
  </tr>


  <tr><td colspan=2><?php echo form_submit('submit', lang('login_submit_btn'));?></td></tr>
</table>
<?php echo form_close();?>

<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>