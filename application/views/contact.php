<div style="color: black; opacity: 1.0; text-align: left;">
<h1>Get In Touch!</h1><br />
If you've got a position you think I might be the right person for, or any consultancy or development enquiries, please just drop me a line via email at:<br /><br />
<h2><a href="mailto: david@dsanders.co.uk">david@dsanders.co.uk</a></h2> <br />
Or, if it's urgent you can always try my mobile on:<br /><br />
<h2><a href="call:07848799645">07848 799645</a></h2> <br />
I'll try my best to answer, but if I'm not available please do leave a message and I'll get back to you as soon as possible.
</div>
