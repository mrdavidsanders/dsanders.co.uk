<div>
<h1>Hello there!</h1><br />
My name is David Sanders. I am a software developer and consultant specialising in PHP, Python and SQL, and I have many years of industry experience at all levels.<br /><br />
If you need a database structure normalised, are thinking of creating a REST API, or just need a reliable developer and consultant then why not take a look at my <a target="_top" href="/work">recent work</a>, or just <a target="_top" href="/contact">get in contact</a>?<br /><br />
If you prefer to connect via LinkedIn, please see my profile <a target="blank" href="http://uk.linkedin.com/in/davidssanders/">here</a>.
</div>
<br />
