<h1>Edit Work Item</h1>

<?php echo validation_errors(); ?>

<?php echo form_open("admin/editWorkItem");?>

      <p>
          Title:<br />
          <input type="text" name="title" value="<?php echo $title; ?>"/>
      </p>

      <p>
          Content:</br />
          <textarea name="content"><?php echo $content; ?></textarea>
      </p>

	  <input type="hidden" name="id" value="<?php echo $id ?>" />
      <p><?php echo form_submit('submit', "Submit");?></p>

<?php echo form_close();?>
