<h1>Add Work Item</h1>

<?php echo validation_errors(); ?>

<?php echo form_open("admin/addWorkItem");?>

      <p>
          Title:<br />
          <input type="text" name="title" value="<?php echo set_value('title'); ?>"/>
      </p>

      <p>
          Content:</br />
          <textarea name="content"><?php echo set_value('content'); ?></textarea>
      </p>


      <p><?php echo form_submit('submit', "Submit");?></p>

<?php echo form_close();?>
