<style>
.work-item {
}
</style>
<div class="work-item" style="color: black; opacity: 1.0; text-align: left;">
<h1>Work Items</h1>
<br /><a href="/admin/addWorkItem">+Add Work Item</a><br /><br />

<table border=0>
<tr><th width=20%>Title</th><th>Content</th><th></th><th></th></tr>
<?php foreach ($work as $news_item): ?>

    <tr>
    	<td><?php echo $news_item['title'] ?></td>
    	<td><?php echo $news_item['content'] ?></td>
    	<td><a href="/admin/editWorkItem/<?php echo $news_item['id'] ?>">e</a></td>
    	<td><a onclick="return verifyDelete();" href="/admin/deleteWorkItem/<?php echo $news_item['id'] ?>">x</a></td>
    </tr>
    
<?php endforeach ?>
</table>

</div>
<br />
<script>
function verifyDelete() {
	var r=confirm("Are you sure?")
	if(r==true){return href;}else{return false;}
}
</script>